package com.example.ilocations.ui

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.*
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.ilocations.R
import com.example.ilocations.data.model.Status
import com.example.ilocations.databinding.ActivityMainBinding
import com.example.ilocations.utils.LocationPermissionHelper
import com.example.ilocations.utils.isGPSEnabled
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionRequest
import com.google.android.gms.location.DetectedActivity
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.Style
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.ref.WeakReference

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), OnIndicatorPositionChangedListener {

    private val mainViewModel: MainViewModel by viewModels()

    private lateinit var locationPermissionHelper: LocationPermissionHelper

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Init data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initUI()
    }

    /** Initialize UI */
    @SuppressLint("SetTextI18n")
    private fun initUI() {
        // Enable user activity listener
        setUserActivityListener()

        // Setup map
        locationPermissionHelper = LocationPermissionHelper(WeakReference(this))
        locationPermissionHelper.checkPermissions {
            onMapReady()
        }

        mainViewModel.getLatestLocation().observe(this, {
            if (it != null) {
                binding.location = Status(
                    latitude = it.latitude,
                    longitude = it.longitude,
                    state = it.state
                )
            }
        })
    }

    private fun onMapReady() {
        mapView.getMapboxMap().setCamera(CameraOptions.Builder().zoom(18.0).build())
        mapView.getMapboxMap().loadStyleUri(Style.MAPBOX_STREETS) {
            mapView.location.apply {
                updateSettings {
                    enabled = true
                    pulsingEnabled = true
                }
                addOnIndicatorPositionChangedListener(this@MainActivity)
            }
        }

        locationAlert()
        startTracking()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationPermissionHelper.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    /** Turn on location */
    private fun locationAlert() {
        if (!isGPSEnabled(this)) {
            showLocationAlertDialog()
        }
    }

    private fun showLocationAlertDialog() {
        AlertDialog.Builder(this).apply {
            setTitle("Location")
            setMessage("GPS is inactive,\nwould like to active it?")
            setPositiveButton("Yes") { dialog, _ ->
                startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                dialog.dismiss()
            }
            setNegativeButton("No") { dialog, _ ->
                dialog.dismiss()
            }
        }.show()
    }

    private fun startTracking() {
        startService(Intent(this, ForeverTracker::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.location.removeOnIndicatorPositionChangedListener(this)
    }

    override fun onIndicatorPositionChanged(point: Point) {
        mapView.getMapboxMap().setCamera(CameraOptions.Builder().center(point).build())
        mapView.gestures.focalPoint = mapView.getMapboxMap().pixelForCoordinate(point)
    }

    /** Set listener for give user activity */
    @SuppressLint("MissingPermission", "UnspecifiedImmutableFlag")
    private fun setUserActivityListener() {
        val transitions = mutableListOf<ActivityTransition>()
        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.IN_VEHICLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.IN_VEHICLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.ON_BICYCLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.ON_BICYCLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.WALKING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.WALKING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.RUNNING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.RUNNING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.STILL)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()

        transitions +=
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.STILL)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()

        val request = ActivityTransitionRequest(transitions)

        val intent = Intent(applicationContext, TransitionReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        ActivityRecognition.getClient(applicationContext)
            .requestActivityTransitionUpdates(request, pendingIntent)
    }

}