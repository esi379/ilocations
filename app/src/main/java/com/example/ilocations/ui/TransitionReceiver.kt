package com.example.ilocations.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.ilocations.data.model.Mode
import com.google.android.gms.location.*

class TransitionReceiver : BroadcastReceiver() {

    companion object {
        var userActivity = Mode.STANDING
    }

    override fun onReceive(context: Context?, intent: Intent) {
        if (ActivityTransitionResult.hasResult(intent)) {
            Log.d("esi2", "ActivityTransitionResult.hasResult")
            val result = ActivityTransitionResult.extractResult(intent)!!
            result.transitionEvents.forEach { event ->
                userActivity = when (event.activityType) {
                    DetectedActivity.WALKING -> Mode.WALKING
                    DetectedActivity.RUNNING -> Mode.RUNNING
                    DetectedActivity.IN_VEHICLE or DetectedActivity.ON_BICYCLE -> Mode.DRIVING
                    else -> Mode.STANDING
                }
            }

            Log.d("esi2", "userActivity: $userActivity")
        }
    }
}