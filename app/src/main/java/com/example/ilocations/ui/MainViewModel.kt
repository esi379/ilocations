package com.example.ilocations.ui

import androidx.lifecycle.ViewModel
import com.example.ilocations.data.model.Status
import com.example.ilocations.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    fun saveLocation(location: Status) = mainRepository.saveCurrentLocation(location)

    fun getLatestLocation() = mainRepository.getLatestUserLocation()
}