package com.example.ilocations.ui

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.example.ilocations.data.model.Status
import com.example.ilocations.data.repository.MainRepository
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ForeverTracker : Service(), LocationListener {

    @Inject
    lateinit var repository: MainRepository

    private lateinit var locationManager: LocationManager

    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationCompatBuilder: NotificationCompat.Builder

    private var currentLocation: Location? = null

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        super.onCreate()

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            500,
            1F,
            this
        )
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.action?.equals(STOP_TRACKING_ACTION) == true) {
            locationManager.removeUpdates(this)
            stopForeground(true)
            stopSelf()
        } else {
            startForeground(NOTIFICATION_ID, generateNotification())
        }

        return START_NOT_STICKY
    }

    private fun generateNotification(): Notification {
        val mainNotificationText =
            "Latitude: ${String.format("%.6f", currentLocation?.latitude ?: 00.00)}\n" +
                    "Longitude: ${String.format("%.6f", currentLocation?.longitude ?: 00.00)}\n" +
                    "Mode: ${TransitionReceiver.userActivity.state}"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID, NOTIF_TITLE, NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val bigTextStyle = NotificationCompat.BigTextStyle()
            .bigText(mainNotificationText)
            .setBigContentTitle(NOTIF_TITLE)

        val launchActivityIntent = Intent(this, MainActivity::class.java)

        val cancelIntent = Intent(this, ForeverTracker::class.java).apply {
            action = STOP_TRACKING_ACTION
        }

        val servicePendingIntent = PendingIntent.getService(
            this, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        val activityPendingIntent = PendingIntent.getActivity(
            this, 0, launchActivityIntent, 0
        )

        notificationCompatBuilder =
            NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)

        return notificationCompatBuilder
            .setStyle(bigTextStyle)
            .setContentTitle(NOTIF_TITLE)
            .setContentText(mainNotificationText)
            .setSmallIcon(com.example.ilocations.R.mipmap.ic_launcher)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setOngoing(true)
            .setOnlyAlertOnce(true)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .addAction(
                com.example.ilocations.R.drawable.ic_launcher_foreground, "Launch app",
                activityPendingIntent
            )
            .addAction(android.R.drawable.ic_delete, "Stop tracking", servicePendingIntent)
            .build()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onLocationChanged(location: Location) {
        currentLocation = location
        CoroutineScope(Dispatchers.IO).launch {
            repository.saveCurrentLocation(
                Status(
                    latitude = currentLocation!!.latitude,
                    longitude = currentLocation!!.longitude,
                    state = TransitionReceiver.userActivity.state
                )
            )
        }

        updateNotification()
    }

    private fun updateNotification() {
        val content =
            "Latitude: ${String.format("%.6f", currentLocation?.latitude ?: 00.00)}\n" +
                    "Longitude: ${String.format("%.6f", currentLocation?.longitude ?: 00.00)}\n" +
                    "Mode: ${TransitionReceiver.userActivity.state}"

        val bigTextStyle = NotificationCompat.BigTextStyle()
            .bigText(content)
            .setBigContentTitle(NOTIF_TITLE)

        notificationCompatBuilder.setStyle(bigTextStyle)
        notificationCompatBuilder.setContentText(content)
        notificationManager.notify(NOTIFICATION_ID, notificationCompatBuilder.build())
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}

    companion object {
        private const val NOTIF_TITLE = "Location Tracker"

        private const val NOTIFICATION_ID = 1410
        private const val NOTIFICATION_CHANNEL_ID = "tracking_channel"

        const val STOP_TRACKING_ACTION = "com.example.ilocations.STOP_TRACKING_ACTION"
    }
}