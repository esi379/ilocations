package com.example.ilocations.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ilocations.data.model.Status

@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocation(location: Status)

    @Query("SELECT * FROM Status ORDER BY id DESC LIMIT 1")
    fun getLastLocation(): LiveData<Status>


}