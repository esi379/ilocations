package com.example.ilocations.data.model

enum class Mode constructor(var state: String) {
    STANDING("Standing"),
    WALKING("Walking"),
    RUNNING("Running"),
    DRIVING("Driving")
}