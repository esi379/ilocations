package com.example.ilocations.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Status(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    var latitude: Double,
    var longitude: Double,
    var state: String
)
