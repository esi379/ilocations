package com.example.ilocations.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.ilocations.data.model.Status

@Database(entities = [Status::class], version = 1)
abstract class AppDB : RoomDatabase() {

    abstract fun locationDao(): LocationDao
}