package com.example.ilocations.data.repository

import com.example.ilocations.data.local.LocationDao
import com.example.ilocations.data.model.Status
import javax.inject.Inject

class MainRepository @Inject constructor(private val locationDao: LocationDao) {

    /** Get the last location of user */
    fun getLatestUserLocation() = locationDao.getLastLocation()

    /** Save current location of user */
    fun saveCurrentLocation(location: Status) = locationDao.insertLocation(location)
}