package com.example.ilocations.di

import android.content.Context
import androidx.room.Room
import com.example.ilocations.data.local.AppDB
import com.example.ilocations.data.local.LocationDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    @Singleton
    internal fun provideAppDB(@ApplicationContext context: Context): AppDB {
        return Room.databaseBuilder(
            context,
            AppDB::class.java,
            "UserLocation.db"
        ).build()
    }

    @Provides
    internal fun provideLocationDao(appDB: AppDB): LocationDao {
        return appDB.locationDao()
    }
}