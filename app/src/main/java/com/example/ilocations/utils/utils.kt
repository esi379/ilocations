package com.example.ilocations.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationManager
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.example.ilocations.data.model.Status

/** Check whether GPS is enabled */
fun isGPSEnabled(context: Context) = (context.getSystemService(Context.LOCATION_SERVICE)
        as LocationManager).isProviderEnabled(LocationManager.GPS_PROVIDER)


/** Format input text before set */
@SuppressLint("SetTextI18n")
@BindingAdapter("formattedText")
fun AppCompatTextView.formattedText(status: Status?) {
    if (status != null) {
        text = "Latitude: ${String.format("%.6f", status.latitude)}\n" +
                "Longitude: ${String.format("%.6f", status.longitude)}\n" +
                "Mode: ${status.state}"
    }
}

