package com.example.ilocations.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.ilocations.R
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkTextViewBackground() {
        onView(allOf(withId(R.id.txt_current_location), isDisplayed()))
            .check { _, _ ->
                matches(hasBackground(R.drawable.bg_location_view))
            }
    }

    @Test
    fun checkTextViewText() {
        val expectedText = "Latitude: 00.00\nLongitude: 00.00\nMode: Standing"
        onView(allOf(withId(R.id.txt_current_location), withText(expectedText)))
    }

}